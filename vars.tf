variable "env_name" {
  default = "production"
}

variable "cf_ac_name" {
  default = "siteac"
}

variable "log_bucket_name" {
  default = "logpablo3"
}

variable "tag_name" {
  default = "site"
}


variable "bucket_name" {
  default = "siteclimapablo"
}



variable "docusaurus_bucket_name" {
  default = "docusaurusblogpablo"
}

variable "docusauruswebsite_bucket_name" {
  default = "docusauruswebsitegpablo"
}