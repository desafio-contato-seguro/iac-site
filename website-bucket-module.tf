resource "aws_s3_bucket" "s3bucketwebsite" {
  bucket = var.bucket_name

  tags = {
    Name        = var.tag_name
    Environment = var.env_name
  }
}


resource "aws_s3_bucket_ownership_controls" "website" {
  bucket = aws_s3_bucket.s3bucketwebsite.id
  rule {
    object_ownership = "BucketOwnerPreferred"
  }
}

resource "aws_s3_bucket_acl" "acl_website" {
  depends_on = [aws_s3_bucket_ownership_controls.website]

  bucket = aws_s3_bucket.s3bucketwebsite.id
  acl    = "private"
}

locals {
  s3_origin_id = "website"
}

