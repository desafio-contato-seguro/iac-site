resource "aws_s3_bucket" "logbucket" {
  bucket = var.log_bucket_name

  tags = {
    Name        = var.tag_name
    Environment = var.env_name
  }
}

resource "aws_s3_bucket_ownership_controls" "logs" {
  bucket = aws_s3_bucket.logbucket.id
  rule {
    object_ownership = "BucketOwnerPreferred"
  }
}

resource "aws_s3_bucket_acl" "acl_logs" {
  depends_on = [aws_s3_bucket_ownership_controls.logs]

  bucket = aws_s3_bucket.logbucket.id
  acl    = "private"
}

locals {
  logs_origin_id = "log"
}
