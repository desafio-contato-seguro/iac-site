terraform {
    backend "s3" {
        bucket         = "pablologs"
        key            = "terraform.tfstate"
        region         = "us-east-1"
        skip_credentials_validation = true
  }
}
