terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
    
  }
}


provider "aws" {}

resource "aws_vpc" "vpc-site" {
  cidr_block = "10.0.0.0/16"
  
}
