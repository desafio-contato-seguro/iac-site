resource "aws_s3_bucket" "docusauruswebsite" {
  bucket = var.docusauruswebsite_bucket_name

  tags = {
    Name        = var.tag_name
    Environment = var.env_name
  }
}


resource "aws_s3_bucket_ownership_controls" "docusauruswebsite" {
  bucket = aws_s3_bucket.docusauruswebsite.id
  rule {
    object_ownership = "BucketOwnerPreferred"
  }
}

resource "aws_s3_bucket_acl" "acl_docusauruswebsite" {
  depends_on = [aws_s3_bucket_ownership_controls.docusauruswebsite]

  bucket = aws_s3_bucket.docusauruswebsite.id
  acl    = "private"
}

locals {
  s3_docusauruswebsite_origin_id = "docusaurus"
}

