# IaC Site

## Resumo

Este repositório contém a Infraestrutura como Código da aplicação Site Clima, disponível [aqui](https://gitlab.com/desafio-contato-seguro/site-clima/)

## Como funciona?

Usando Terraform para escrever os códigos, são criados dois buckets, um para armazenar os códigos do Site Clima e o outro para armazenar os logs do CloudFront, que também é criado através do Terraform. 

A configuração do CloudFront foi feita de forma que impossibilitasse acessos de usuários que estiverem fora do Brasil ou usando VPNs ou outras ferramentas que possibilitam mudar o IP de Origem do Cliente para um que não seja do Brasil.

A aplicação dos códigos, assim como a validação e o planejamento, é feita através de uma pipeline contida no arquivo de CI do GitLab. Neste repositório estão definidas também as variáveis da AWS que o Terraform necessita para executar a construção dos recursos.
