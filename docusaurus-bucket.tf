resource "aws_s3_bucket" "docusaurus" {
  bucket = var.docusaurus_bucket_name

  tags = {
    Name        = var.tag_name
    Environment = var.env_name
  }
}


resource "aws_s3_bucket_ownership_controls" "docusaurus" {
  bucket = aws_s3_bucket.docusaurus.id
  rule {
    object_ownership = "BucketOwnerPreferred"
  }
}

resource "aws_s3_bucket_acl" "acl_docusaurus" {
  depends_on = [aws_s3_bucket_ownership_controls.docusaurus]

  bucket = aws_s3_bucket.docusaurus.id
  acl    = "private"
}

locals {
  s3_docusaurus_origin_id = "docusaurus"
}

